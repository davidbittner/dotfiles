"Autodownload plug.vim if it does not exist
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"Start plugins
call plug#begin('~/.config/nvim/bundle/')

"Smoooooooth airline bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
"Rust integration
Plug 'rust-lang/rust.vim'
"Colorscheme
Plug 'arcticicestudio/nord-vim'
"ctrlp
Plug 'ctrlpvim/ctrlp.vim'
"Goyo
Plug 'junegunn/goyo.vim'
"CoC (for completion)
Plug 'DingDean/wgsl.vim'

"GLSL Highlighting
Plug 'tikhomirov/vim-glsl'

" Collection of common configurations for the Nvim LSP client
Plug 'neovim/nvim-lspconfig'
" Test framework support
Plug 'vim-test/vim-test'
" Autocompletion framework
Plug 'hrsh7th/nvim-cmp'
" cmp LSP completion
Plug 'hrsh7th/cmp-nvim-lsp'
" cmp Snippet completion
Plug 'hrsh7th/cmp-vsnip'
" cmp Path completion
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-buffer'

" Adds extra functionality over rust analyzer
Plug 'simrat39/rust-tools.nvim'

" Snippet engine
Plug 'hrsh7th/vim-vsnip'

call plug#end()

source ~/.config/nvim/rust-analyzer-lsp.vim

"airline
let g:airline#extensions#ale#enabled = 1
let g:airline_theme='nord'

"coc
set hidden
set nobackup
set nowritebackup

set cmdheight=2
set updatetime=300
set shortmess+=c
set signcolumn=yes

"CtrlP
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']
let g:ctrlp_cmd='CtrlP'
