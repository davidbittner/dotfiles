"                    ,--.           
"   ,--,--,,--.  ,--.`--',--,--,--. 
"   |      \\  `'  / ,--.|        | 
"   |  ||  | \    /  |  ||  |  |  | 
"   `--''--'  `--'   `--'`--`--`--' 
"

set shell=/bin/zsh

source ~/.config/nvim/plugins.vim
source ~/.config/nvim/keymaps.vim
source ~/.config/nvim/commands.vim

"Hybrid line numbers, syntax higlighting
set nu
set relativenumber
syntax on
colorscheme nord

"editor preferences
set tabstop=4 
set shiftwidth=4
set expandtab "don't talk to me if you use tabs
set colorcolumn=80
set nocompatible

"Makefiles
autocmd FileType make setlocal noexpandtab
autocmd FileType json setlocal smarttab
autocmd FileType go   setlocal noexpandtab
