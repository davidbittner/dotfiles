#!/bin/bash

if hash acpi 2>/dev/null; then
    echo `acpi -b | cut -d" " -f 3-7`
else
    exit 0
fi
