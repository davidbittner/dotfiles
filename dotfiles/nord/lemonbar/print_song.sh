#!/bin/bash

xg() {
    xgetres "$@"
}

PLAYING='>'
PAUSED='||'


STYLE="%{U`xg color0`}%{F`xg background`}%{B`xg color2`}%{+u}%{+o}"
 POST="%{-u}%{-o}%{U-}%{B-}%{F-}"

WIDTH=40

playerctl status > /dev/null 2>&1

if [[ $? -ne 1 ]]; then
    STATUS=`playerctl status`

    ARTIST=`playerctl metadata artist`
      SONG=`playerctl metadata title`

    case "$STATUS" in
        "Playing")
            PRINT="$PLAYING $ARTIST - $SONG"
            ;;

        "Paused")
            PRINT="$PAUSED $ARTIST - $SONG"
            ;;
    esac
    if [[ `echo "$PRINT" | wc -m` -gt $WIDTH ]]; then
        PRINT=`printf "%.$WIDTH""s..." "$PRINT"`
    fi

    printf "%%{B`xg color0`} %%{B-}"
    printf "%s$WIDGET_PADDING%s$WIDGET_PADDING%s" "$STYLE" "$PRINT" "$POST"
    printf "%%{B`xg color0`} %%{B-}"
fi

exit 0
