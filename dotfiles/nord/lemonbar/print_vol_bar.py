#!/bin/python

import argparse
import sys
import signal

import pulsectl

BAR_WIDTH = 10

class SoundHandler:
    def __init__(self):
        self._pulse = pulsectl.Pulse('vol-client-polybar')
        self._sink  = self._pulse.sink_list()[-1]
        self._kill = False

        signal.signal(signal.SIGINT, self.is_kill)
        signal.signal(signal.SIGTERM, self.is_kill)

    def _print_vol_status(self):
        cur_vol = self._sink.volume.value_flat

        cur_vol = int(cur_vol*float(BAR_WIDTH))

        print("VOL ", end="")

        print("∣", end="")

        for i in range(0, BAR_WIDTH+1):
            if i is cur_vol:
                if self._sink.mute:
                    print("―", end="")
                else:
                    print(" ∣", end="")
            else:
                print("―", end="")

        print("∣")

        sys.stdout.flush()

    def is_kill(self, signum, frame):
        self._kill = True

SoundHandler()._print_vol_status()
