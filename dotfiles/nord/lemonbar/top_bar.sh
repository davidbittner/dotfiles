#!/bin/bash

xg() {
    xgetres "$@"
}

BACKGROUND=`xg background`
FOREGROUND=`xg foreground`

FONTS=`xg config.font`
BORDER_WIDTH=`xg config.border_size`

FONT_LIST=""

for font in $FONTS; do
    FONT_LIST+="-f "
    FONT_LIST+="$font "
done

lemonbar -B $BACKGROUND -F $FOREGROUND $FONT_LIST -u $BORDER_WIDTH -g "1920x32+0+0" -o -5
