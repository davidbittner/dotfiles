#!/bin/bash

xg() {
    xgetres "$@"
}

VOLUME_STR=`$HOME/.config/lemonbar/print_vol_bar.py`
STYLE="%{U`xg color0`}%{F`xg background`}%{B`xg color1`}%{+u}%{+o}"
 POST="%{-u}%{-o}%{U-}%{B-}%{F-}"

printf "%%{B`xg color0`} %%{B-}"
printf "%s$WIDGET_PADDING%s$WIDGET_PADDING%s" "$STYLE" "$VOLUME_STR" "$POST"
printf "%%{B`xg color0`} %%{B-}"
