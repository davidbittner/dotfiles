#!/bin/bash

killall -q lemonbar

while pgrep -u $UID -x lemonbar > /dev/null; do sleep 1; done

. $HOME/.config/lemonbar/top_bar_stdout.sh | $HOME/.config/lemonbar/top_bar.sh
