#!/bin/bash
xg() {
    xgetres "$@"
}

DATE=`date "+%b %d, %r"`
WIDTH=10

STYLE="%{U`xg color0`}%{F`xg background`}%{B`xg color11`}%{+u}%{+o}"
 POST="%{-u}%{-o}%{U-}%{B-}%{F-}"

printf "%%{B`xg color0`} %%{B-}"
printf "%s$WIDGET_PADDING%"$WIDTH"s$WIDGET_PADDING%s" "$STYLE" "$DATE" "$POST"
printf "%%{B`xg color0`} %%{B-}"
