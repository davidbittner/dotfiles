#!/bin/bash

xg() {
    xgetres "$@"
}

export PYTHONIOENCODING=utf8
export WIDGET_PADDING=" "
export WIDGET_MARGINS="  "

WIDGET_EDGES="   "

while true; do
    MUSIC_WIDGET=`$HOME/.config/lemonbar/print_song.sh 2>/dev/null`
    
    L_WIDGETS="$WIDGET_MARGINS$MUSIC_WIDGET$WIDGET_MARGINS"
    C_WIDGETS=""
    R_WIDGETS=\
"\
`. $HOME/.config/lemonbar/print_volume.sh`$WIDGET_MARGINS\
`. $HOME/.config/lemonbar/print_battery.sh`$WIDGET_MARGINS\
`. $HOME/.config/lemonbar/print_date.sh`$WIDGET_MARGINS"

    printf "%%{l}$WIDGET_EDGES%s %%{c}%s %%{r}%s$WIDGET_EDGES\n" "${L_WIDGETS[*]}" "${C_WIDGETS[*]}" "${R_WIDGETS[*]}"

    sleep 0.5
done
