#!/bin/bash

ON="%{F`xgetres color15`}|-O|%{F-}"
OFF="%{F`xgetres color1`}|O-|%{F-}"

while true; do
    echo -n "VPN: "

    nordvpn status | grep Status | grep -q "Connected"
    if [[ $? -eq 0 ]]; then
        echo $ON
    else
        echo $OFF
    fi

    sleep 1
done
