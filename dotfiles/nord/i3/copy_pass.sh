#!/bin/bash
PASS=$(wofi --dmenu -P --lines=2 -b)

if [ -z "$PASS" ]; then
    exit 1
fi

OUTPUT=$(bw unlock $PASS)
if [ $? -eq 1 ]; then
    notify-send "Invalid password."
    exit 1
else
    SESSION=$(echo "$OUTPUT" | sed "4q;d" | awk '{print $3}')
    SESSION=$(echo "$SESSION" | sed "s/BW_SESSION=//g")

    NAMES=$(bw list items --session $SESSION | jq ".[] | select(.type == 1) | .name" | sed "s/\"//g" )
    RESP=$(echo "$NAMES" | wofi --dmenu -p "Entry Name:")

    if [ -z $RESP ]; then
        exit 0
    fi

    PASS=$(bw list items --session $SESSION | jq ".[] | select(.name == \"$RESP\") | .login.password" | sed "s/\"//g")
    if which xclip; then
        echo "Copying using xclip..."
        echo "$PASS" | xclip -selection "clipboard"
    fi
    if which wl-copy; then
        echo "Copying using wl-copy..."
        wl-copy "$PASS"
    fi

    notify-send "Password for $RESP copied successfully."
fi
